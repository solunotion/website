from flask import jsonify
import hashlib, binascii, os


from config.dbConn import Db
from classes.statuscodes import StatusCodes

class SendServices:
    base_img_url = "https://drive.google.com/uc?export=view&id="

    @staticmethod
    def hash_pp(data):
        #salt = os.urandom(32)
        salt = "SOLUNOTIONcyril".encode()
        result1 = data.encode()
        #result2 = hashlib.md5('Passwordfile2'.encode())

        #return binascii.hexlify(result1)
        #save1now = result1.hexdigest()
        #save2now = result2.hexdigest()
        #return binascii.hexlify(salt)
        keyvalue = hashlib.pbkdf2_hmac('sha256', # The hash digest algorithm for HMAC
            result1, # Convert the password to bytes
            salt, # Provide the salt
            100000 # It is recommended to use at least 100,000 iterations of SHA-256 
        )
        return {
            "salt": binascii.hexlify(salt).decode(),
            "keyvalue": binascii.hexlify(keyvalue).decode()
        }
    
    @staticmethod
    def verify_pp(salt, stored_pp, provided_pp):
        check_keyvalue = hashlib.pbkdf2_hmac('sha256', # The hash digest algorithm for HMAC
            provided_pp.encode(), # Convert the password to bytes
            salt, # Provide the salt
            100000 # It is recommended to use at least 100,000 iterations of SHA-256
        )
        if(stored_pp == check_keyvalue):
            return "Yes we are the same"
    
    @staticmethod
    def closeConnection(mydb, mycursor):
        if(mydb.is_connected()):
            mycursor.close()
            mydb.close()
    
    @staticmethod
    def getroles():
        try:
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT \
                    uroles.id AS role_id,\
                    uroles.title AS role_title,\
                    upermissions.desc AS role_permission,\
                    upermissions.id AS role_permission_id\
                    FROM uroles\
                    JOIN upermissions ON uroles.upermission_id = upermissions.id ORDER BY uroles.title"
            #val = ('1')
            mycursor.execute(sql)
            myresult = mycursor.fetchall()

            if mycursor.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "data": {
                        "roles": myresult
                    }
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "No role available yet, contact admin to create",
                    "data": myresult
                }
                return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"
        except:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry your sql"
            }
            return jsonify(response)
        finally:
            if(mydb.is_connected()):
                mycursor.close()
                mydb.close()
                #return "MySQL connection is closed"

    @staticmethod
    def getcategories():
        try:
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT categories.id, categories.title, categories.desc, categories.slug, categories.parent_cat_id FROM categories WHERE categories.status = 0 ORDER BY categories.title"
            #val = ('1')
            mycursor.execute(sql)
            myresult = mycursor.fetchall()

            if mycursor.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "data": {
                        "cat": myresult
                    }
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "No parent category available yet, you can create one",
                    "data": myresult
                }
                return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"
        except:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry your sql"
            }
            return jsonify(response)
        
        finally:
            if(mydb.is_connected()):
                mycursor.close()
                mydb.close()
                #return "MySQL connection is closed"

    @staticmethod
    def getpermissions():
        try:
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT upermissions.id, upermissions.title, upermissions.desc FROM upermissions WHERE upermissions.status = 0 ORDER BY upermissions.title"
            #val = ('1')
            mycursor.execute(sql)
            myresult = mycursor.fetchall()

            if mycursor.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "data": {
                        "perms": myresult
                    }
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "No Permissions available yet, you can create one",
                    "data": myresult
                }
                return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"
        except:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry your sql"
            }
            return jsonify(response)
        
        finally:
            if(mydb.is_connected()):
                mycursor.close()
                mydb.close()
                #return "MySQL connection is closed"

    @staticmethod
    def getusers(which_option, data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        if which_option == "all":
            try:
                per_page = int(20)
                page_limit = int(per_page * int(data['v_page_nos']))
                v_num = int(0)

                #return jsonify(data)

                
                sql = "SELECT \
                        users.id AS user_id,\
                        users.fullname AS user_fullname,\
                        users.email AS user_email,\
                        users.phone AS user_phone,\
                        users.img AS user_img,\
                        uroles.id AS user_role_id,\
                        uroles.title AS user_role_title\
                        FROM users\
                        JOIN uroles ON users.urole_id = uroles.id \
                        WHERE users.id >= " + str(v_num) + " AND users.status = 0 ORDER BY users.fullname LIMIT " + str(page_limit)
                #val = ('1')
                mycursor.execute(sql)
                myresult = mycursor.fetchall()

                if mycursor.rowcount > 0:
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Operation successful",
                        "data": {
                            "users": myresult
                        }
                    }
                    return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "No User available yet, kindly contact the admin",
                        "data": myresult
                    }
                    return jsonify(response)

                #for x in myresult:
                #   print(x)
                #return "Yes you are in about page"
            except:
                response = {
                    "status": "error",
                    "code": 100,
                    "message": "Sorry your sql"
                }
                return jsonify(response)
            
            finally:
                if(mydb.is_connected()):
                    mycursor.close()
                    mydb.close()
                    #return "MySQL connection is closed"

        if which_option == "byrole":
            try:
                per_page = int(20)
                page_limit = int(per_page * int(data['v_page_nos']))

                #return page_limit
                g_from = int(data['g_from'])
                d_role = int(data['role_id'])

                sql = "SELECT \
                        users.id AS user_id,\
                        users.fullname AS user_fullname,\
                        users.email AS user_email,\
                        users.phone AS user_phone,\
                        users.img AS user_img,\
                        uroles.id AS user_role_id,\
                        uroles.title AS user_role_title\
                        FROM users\
                        JOIN uroles ON users.urole_id = uroles.id \
                        WHERE users.id >= " + str(g_from) + " AND users.urole_id = " + str(d_role) + " AND users.status = 0 ORDER BY users.fullname LIMIT " + str(page_limit)
                #val = ('1')
                mycursor.execute(sql)
                myresult = mycursor.fetchall()

                if mycursor.rowcount > 0:
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Operation successful",
                        "data": {
                            "users": myresult
                        }
                    }
                    return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "No User available yet, kindly contact the admin",
                        "data": myresult
                    }
                    return jsonify(response)

                #for x in myresult:
                #   print(x)
                #return "Yes you are in about page"
            except:
                response = {
                    "status": "error",
                    "code": 100,
                    "message": "Sorry your sql"
                }
                return jsonify(response)
            
            finally:
                if(mydb.is_connected()):
                    mycursor.close()
                    mydb.close()
                    #return "MySQL connection is closed"


    @staticmethod
    def getpost(which_post, page_num):
        ourimg = SendServices.base_img_url

        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        if page_num:
            if which_post == "all":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    #stopped here...
                    per_page = int(20)
                    page_limit = int(per_page * int(page_num['v_page_nos']))
                    v_num = int(0)
                    #return v_num

                    sql = "SELECT \
                        posts.id AS post_id, \
                        posts.title AS post_title,\
                        posts.desc AS post_desc,\
                        posts.created_at AS post_date,\
                        posts.img AS post_img,\
                        categories.title AS post_category,\
                        categories.id AS post_category_id\
                        FROM posts\
                        JOIN categories ON posts.cat_id = categories.id\
                        WHERE posts.id >= " + str(v_num) + " AND posts.status = 0 ORDER BY posts.id DESC LIMIT " + str(page_limit)

                    mycursor.execute(sql)
                    myresult = mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        mycursor.execute("SELECT COUNT(*) FROM posts WHERE posts.id >="+str(v_num))
                        rcount = mycursor.fetchone()['COUNT(*)']

                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Post gotten successfully",
                            "data": {
                                "works": myresult,
                                "count": rcount,
                                "last_post": myresult[0]['post_id'],
                                "base_img_url": ourimg
                            }
                            
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no post available"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

            if which_post == "recent_work":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    #stopped here...

                    v_num = int(page_num['g_from'])

                    #v_num = int("0")
                    #return v_num

                    sql = "SELECT \
                        posts.id AS post_id, \
                        posts.title AS post_title,\
                        posts.desc AS post_desc,\
                        posts.img AS post_img,\
                        categories.title AS post_category, \
                        categories.id AS category_id,\
                        categories.desc AS category_desc\
                        FROM posts\
                        JOIN categories ON posts.cat_id = categories.id\
                        WHERE posts.id > " + str(v_num) + " AND posts.status = 0 ORDER BY posts.id DESC LIMIT 3"

                    ##return sql

                    #sql = "SELECT posts.id, posts.title, posts.desc, posts.img, posts.cat_id FROM posts WHERE posts.status = 0 ORDER BY posts.id"
                    mycursor.execute(sql)
                    myresult = mycursor.fetchmany(6)
                    
                    ##return mycursor.rowcount

                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Post gotten successfully",
                            "data": {
                                "works": myresult,
                                "base_img_url": ourimg
                            },
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no post available"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry check your sql"
                    }
                    return response
                
                finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

            if which_post == "bycat":
                #return page_num['g_from']
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    #stopped here...
                    per_page = int(20)
                    page_limit = int(per_page * int(page_num['v_page_nos']))

                    #return page_limit
                    g_from = int(page_num['g_from'])
                    d_cat = int(page_num['cat_id'])

                    sql = "SELECT \
                        posts.id AS post_id, \
                        posts.title AS post_title,\
                        posts.desc AS post_desc,\
                        posts.created_at AS post_date,\
                        posts.img AS post_img,\
                        categories.title AS post_category,\
                        categories.id AS post_category_id\
                        FROM posts\
                        JOIN categories ON posts.cat_id = categories.id\
                        WHERE posts.id >= " + str(g_from) + " AND posts.cat_id = " + str(d_cat) + " AND posts.status = 0 ORDER BY posts.id DESC LIMIT " + str(page_limit)

                    mycursor.execute(sql)
                    myresult = mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        mycursor.execute("SELECT COUNT(*) FROM posts WHERE posts.id >= " + str(g_from) + " AND posts.cat_id = " + str(d_cat) )
                        rcount = mycursor.fetchone()['COUNT(*)']

                        if rcount > 0:
                            response = {
                                "status": "success",
                                "code": 200,
                                "message": "Post gotten successfully",
                                "data":{
                                    "works": myresult,
                                    "count": rcount,
                                    "selected_cat": d_cat,
                                    "last_post": myresult[0]['post_id'],
                                    "base_img_url": ourimg
                                }
                            }
                            return response
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "There is no post to this category yet"
                            }
                            return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no post available to this category..."
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"


        else:
            return jsonify({
                "status": "error",
                "code": 100,
                "message": "Hey you getting something wrong"
            })
    
    @staticmethod
    def checkDataExist(field, tb, data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        if tb == "users":
            if field == "id":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT users.id FROM users WHERE users.id = %s AND users.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 100,
                            "message": "User Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

            if field == "urole_id":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT users.email FROM users WHERE users.urole_id = %s AND users.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 100,
                            "message": "User Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"


            if field == "email":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT users.email FROM users WHERE users.email = %s AND users.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 100,
                            "message": "User Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

        if tb == "categories":
            if field == "slug":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT categories.slug FROM categories WHERE categories.slug = %s AND categories.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 100,
                            "message": "User Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

        if tb == "posts":
            if field == "cat_id":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT posts.title FROM posts WHERE posts.cat_id = %s AND posts.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Post Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Post Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"


    @staticmethod
    def insert(string, data):    
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        if string == "createpost":
            #return "Hey"
            ##return jsonify(data)

            sql = "INSERT INTO posts (posts.title, posts.desc, posts.img, posts.cat_id, posts.status) VALUES (%s, %s, %s, %s, %s)"
            val = (data['title'], str(data['desc']), data['img'], data['cat_id'], 0)
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Post Created Successfully"
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Post NOT created"
                }
                return jsonify(response)
        
        if string == "createuser":
            
            checkresponse = SendServices.checkDataExist('email', 'users', data['email'])

            #return jsonify(checkresponse)

            if checkresponse['status'] == "success":
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Sorry User already exist"
                }
                return jsonify(response)
            else:
                pp_hash = SendServices.hash_pp(data['pword'])
                
                mydb = Db.dbfun()
                mycursor = mydb.cursor()
                sql = "INSERT INTO users (users.fullname, users.email, users.phone, users.urole_id, users.key_value, users.salt) VALUES (%s, %s, %s, %s, %s, %s)"
                val = (data['fullname'], data['email'], data['phone'], data['role'], pp_hash['keyvalue'], pp_hash['salt'])
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "User Created Successfully" 
                    }
                    return jsonify(response)                    
                else:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry an Error Occured Contact the Admin"
                    }
                    return jsonify(response)

        if string == "createcat":
            
            checkresponse = SendServices.checkDataExist('slug', 'categories', data['slug'])

            if checkresponse['code'] == 200:
                response = {
                    "code": 400,
                    "status": "error",
                    "message": "Sorry Category with same slug already exist"
                }
                return jsonify(response)
            else:
                sql = "INSERT INTO categories (categories.title, categories.desc, categories.slug, categories.parent_cat_id) VALUES (%s, %s, %s, %s)"
                val = (data['title'], data['desc'], data['slug'], data['parent_cat_id'])
                
                mycursor.execute(sql, val)

                mydb.commit()
                if mycursor.rowcount > 0 :
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Category created successfully"
                    }
                    return jsonify(response)                
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Category not created"
                    }
                    return jsonify(response)
            
    @staticmethod
    def update(string, data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        if string == "deleteuser":
            sql = "UPDATE users SET users.status = 1 WHERE users.id = %s"
            val = (data['user_id'], )
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "User Deleted Successfully"
                }
                return jsonify(response)
                
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "User not deleted successfully"
                }
                return jsonify(response)
        

        if string == "deletepost":
            sql = "UPDATE posts SET posts.status = 1 WHERE posts.id = %s"
            val = (data['post_id'], )
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Post Deleted Successfully"
                }
                return jsonify(response)
                
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Post not deleted successfully"
                }
                return jsonify(response)
        
        if string == "updatepost":
            sql = "UPDATE posts SET posts.title = %s, posts.desc = %s, posts.img = %s, posts.cat_id = %s, posts.status = %s WHERE posts.id = %s"
            val = (data['title'], data['desc'], data['img'], data['cat_id'], 0, data['post_id'])
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Post Updated Successfully"
                }
                return jsonify(response)
                
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Post not updated successfully, seems no changes was made. Ensure you are doing the right thing"
                }
                return jsonify(response)
        
        if string == "updateuser":
            checkresponse = SendServices.checkDataExist('id', 'users', data['user_id'])

            if checkresponse['code'] == 200:
                val_len = len(str(data['user_pwd']))
                
                if val_len > 5:
                    pp_hash = SendServices.hash_pp(data['user_pwd'])
                    sql = "UPDATE users SET users.fullname = %s, users.email = %s, users.phone = %s, users.key_value = %s WHERE users.id = %s AND users.status = %s"
                    val = (data['user_fullname'], data['user_email'], data['user_phone'], pp_hash['keyvalue'], data['user_id'], 0)
                else:
                    sql = "UPDATE users SET users.fullname = %s, users.email = %s, users.phone = %s WHERE users.id = %s AND users.status = %s"
                    val = (data['user_fullname'], data['user_email'], data['user_phone'], data['user_id'], 0)
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "User Updated Successfully"
                    }
                    return jsonify(response)
                    
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "User didn't update, seems no changes was made. Ensure you are doing the right thing"
                    }
                    return jsonify(response)
            
        if string == "updatecat":
            sql = "UPDATE categories SET categories.title = %s, categories.desc = %s, categories.slug = %s, categories.parent_cat_id = %s, categories.status = %s WHERE categories.id = %s"
            val = (data['title'], data['desc'], data['slug'], data['parent_cat_id'], 0, data['id'])
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Category Updated Successfully"
                }
                return jsonify(response)
                
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Category didn't updated, seems no changes was made. Ensure you are doing the right thing"
                }
                return jsonify(response)
        
        if string == "updaterole":
            sql = "UPDATE uroles SET uroles.title = %s, uroles.upermission_id = %s WHERE uroles.id = %s"
            val = (data['role_title'], data['role_permission_id'], data['role_id'])
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Role Updated Successfully"
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Role didn't update, seems no changes was made. Ensure you are doing the right thing"
                }
                return jsonify(response)
        
        if string == "deletecat":
            cresponse = SendServices.checkDataExist('cat_id', 'posts', data['id'])

            #return jsonify(cresponse)

            if cresponse['status'] == "success":
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Sorry this category already has a post, and can't be deleted"
                }
                return jsonify(response)
            else:
                sql = "UPDATE categories SET categories.status = 1 WHERE categories.id = %s"
                val = (data['id'], )
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Category Deleted Successfully"
                    }
                    return jsonify(response)
                    
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Category not deleted successfully"
                    }
                    return jsonify(response)

        if string == "deleterole":
            cresponse = SendServices.checkDataExist('urole_id', 'users', data['role_id'])
            #return jsonify(cresponse)

            if cresponse['status'] == "success":
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Sorry this category already has a post, and can't be deleted"
                }
                return jsonify(response)
            else:
                sql = "UPDATE uroles SET uroles.status = 1 WHERE uroles.id = %s"
                val = (data['role_id'], )
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Role Deleted Successfully"
                    }
                    return jsonify(response)
                    
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Role not deleted"
                    }
                    return jsonify(response)

        if string == "savesettings":
            mydb = Db.dbfun()
            mycursor = mydb.cursor()
            
            sql = "UPDATE settings SET settings.app_name = %s, settings.app_motto = %s, settings.app_why_title = %s, settings.app_why_desc = %s WHERE settings.id = %s"
            val = (data['bus_name'], data['bus_motto'], data['bus_why_title'], data['bus_why_desc'], 1)
            
            mycursor.execute(sql, val)

            mydb.commit()
            if mycursor.rowcount > 0 :
                response = {
                    "status": "success",
                    "code": 200,  
                }
                return jsonify(response)
                
            else:
                response = {
                    "status": "error",
                    "code": 400
                }
                return jsonify(response)

    @staticmethod
    #data = {"g_from": "0"}
    def select(data):
        string = data['string']
        
        if string == "/":
            r_data = SendServices.getpost("recent_work", data)
            return jsonify(r_data)
        
        if string == "ourwork":
            r_data = SendServices.getpost("all", data)
            return jsonify(r_data)
        
        if string == "ourworkbycat":
            the_cat = int(data['cat_id'])
            if the_cat == 0:
                r_data = SendServices.getpost("all", data)
                return jsonify(r_data)
            else:
                r_data = SendServices.getpost("bycat", data)
                return jsonify(r_data)
        
        if string == "usersbyrole":
            the_role = int(data['role_id'])
            if the_role == 0:
                return SendServices.getusers("all", data)
            else:
                return SendServices.getusers("byrole", data)
        

        if string == "getroles":
            return SendServices.getroles()
        
        if string == "getusers":
            return SendServices.getusers("all", data)

        if string == "getcat":
            return SendServices.getcategories()
        
        if string == "getperms":
            return SendServices.getpermissions()

        if string == "getparentcat":
           return SendServices.getcategories()

        if string == "adminlogin":
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            try:
                #data_e = 'thankgoduchecyril@gmail.com'
                #stopped here...
                #return v_num
                ##u_pp = SendServices.verify_pp(data['pword'])
                #return jsonify(str(u_pp))
                pp_hash = SendServices.hash_pp(data['pword'])

                #return jsonify(pp_hash)
                sql = "SELECT \
                    users.id AS id, \
                    users.fullname AS fullname,\
                    users.email AS email,\
                    users.phone AS phone,\
                    users.img AS img,\
                    uroles.title AS role,\
                    uroles.id AS role_id\
                    FROM users\
                    JOIN uroles ON users.urole_id = uroles.id\
                    WHERE users.email = %s AND users.key_value = %s AND users.salt = %s AND users.status = %s" 
                
                val = (data['uname'], pp_hash['keyvalue'], pp_hash['salt'], 0)
                mycursor.execute(sql, val)
                myresult = mycursor.fetchone()
                
                if mycursor.rowcount > 0:
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Login successful",
                        "data": {
                            "user": myresult
                        }
                        
                    }
                    return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry ensure that your username and password are correct"
                    }
                    return jsonify(response)
            except:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Please contact the Admin to rectify this error"
                }
                return jsonify(response)
            
            finally:
                SendServices.closeConnection(mydb, mycursor)
                #return "MySQL connection is closed"

        if string == "getsettings":
                mydb = Db.dbfun()
                mycursor = mydb.cursor(dictionary=True)

                sql = "SELECT settings.app_name, settings.app_motto, settings.app_why_title, settings.app_why_desc FROM settings WHERE settings.id = %s"
                val = ("1",)
                mycursor.execute(sql, val)
                myresult = mycursor.fetchall()

                if mycursor.rowcount > 0:
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Operation successful",
                        "data": {
                            "bus_name": myresult[0]['app_name'],
                            "bus_motto": myresult[0]['app_motto'],
                            "bus_why_title": myresult[0]['app_why_title'],
                            "bus_why_desc": myresult[0]['app_why_desc']
                        }
                    }
                    return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Operation Successful, but no parent category available yet, you can create one",                        
                        "data": myresult

                    }
                    return jsonify(response)

                #for x in myresult:
                #   print(x)
                #return "Yes you are in about page"
        
        if string == "mydashboard":
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)
            #return page_num['g_from']
            try:
                mycursor.execute("SELECT COUNT(*) FROM posts WHERE posts.status = 0")
                pcount = mycursor.fetchone()['COUNT(*)']

                mycursor.execute("SELECT COUNT(*) FROM categories WHERE categories.status = 0")
                ccount = mycursor.fetchone()['COUNT(*)']

                mycursor.execute("SELECT COUNT(*) FROM users WHERE users.status = 0")
                ucount = mycursor.fetchone()['COUNT(*)']

                if pcount > 0:
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Post gotten successfully",
                        "data":{
                            "post_count": pcount,
                            "cat_count": ccount,
                            "user_count": ucount
                        }
                    }
                    return jsonify(response)
                else:
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Post gotten successfully",
                        "data":{
                            "post_count": pcount,
                            "cat_count": ccount,
                            "user_count": ucount
                        }
                    }
                    return jsonify(response)
            except:
                response = {
                    "status": "error",
                    "code": 100,
                    "message": "Sorry your sql"
                }
                return jsonify(response)
            
            finally:
                    SendServices.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"