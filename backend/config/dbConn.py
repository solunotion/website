import mysql.connector
from mysql.connector import Error

class Db:
	@staticmethod
	def dbfun():
		try:
			mydb = mysql.connector.connect(
				host="localhost",
				#user="suntsjjc_solunotion_admin",
				user="root",
				##for live server user is suntsjjc_solunotion_admin
				#passwd="adminadmin@solunotion",
				passwd="adminadmin@root",
				##for live password is adminadmin@solunotion
				database= "solunotion_website"
				#database= "suntsjjc_solunotion_website"
				##for live database is suntsjjc_solunotion_website
				##when importing database remember to take note of this
				##In order to import your database file, it is required to open the dump.sql file in Notepad++ and hit CTRL+H to find and replace the string “utf8mb4_0900_ai_ci” and replace it with “utf8mb4_general_ci“.
			)

			if mydb.is_connected():
				##db_info = mydb.get_server_info()
				
				return mydb
					##print(mydb)
					##mycursor = mydb.cursor()
					##mycursor.execute("SHOW DATABASES")
					##alldata = mycursor.fetchall()
					##for x in alldata:
					##print(x)
			else:
				print("we are not connecting")
		except Error as e:
			print(e)