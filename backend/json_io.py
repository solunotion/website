import sys
sys.path.insert(0, '/home/suntsjjc/solunotion.com/backend/')

#system library imports
import json

#third party imports
from flask import Flask, redirect, url_for, request
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)

#local imports
from mainbase.BaseController import BaseClass


main_error = "Hey, What are you doing"
##print(main_error)
##redirect(url_for('success',name = 'yes it is working'))


#routes
@app.route('/', methods=['POST'])
#@cross_origin()
def homePage():
    ##return redirect(url_for('success',name = 'yes it is working'))
    ##return main_error
    if request.data:
        i_data = json.loads(request.data)
        #return request.data
        return BaseClass.homepage(i_data)
    else:
        return main_error

@app.route('/ourworkbycat', methods=['POST'])
def workPageByCat():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.workpagebycat(i_data)
    else:
        return main_error

@app.route('/usersbyrole', methods=['POST'])
def usersByRole():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.usersbyrole(i_data)
    else:
        return main_error

@app.route('/mydashboard', methods=['POST'])
def mydashboard():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.mydashboard(i_data)
    else:
        return main_error


@app.route('/ourwork', methods=['POST'])
def workPage():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.workpage(i_data)
    else:
        return main_error

@app.route('/getparentcat', methods=['POST'])
def get_parentcat():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.getparentcat(i_data)
    else:
        return main_error

@app.route('/getroles', methods=['POST'])
def get_roles():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.getroles(i_data)
    else:
        return main_error

@app.route('/getusers', methods=['POST'])
def get_users():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.getusers(i_data)
    else:
        return main_error

@app.route('/getperms', methods=['POST'])
def get_perms():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.getperms(i_data)
    else:
        return main_error

@app.route('/getcat', methods=['POST'])
def get_cat():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.getcat(i_data)
    else:
        return main_error

@app.route('/getsettings', methods=['POST'])
def get_settings():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.getsettings(i_data)
    else:
        return main_error

@app.route('/adminlogin', methods=['POST'])
def admin_login():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.adminlogin(i_data)
    else:
        return main_error



@app.route('/savesettings', methods=['POST'])
def save_settings():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.savesettings(i_data['string'], i_data)
    else:
        return main_error

@app.route('/createcat', methods=['POST'])
def create_category():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.createcat(i_data['string'], i_data)
    else:
        return main_error

@app.route('/createpost', methods=['POST'])
def create_post():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.createpost(i_data['string'], i_data)
    else:
        return main_error

@app.route('/updatepost', methods=['POST'])
def update_post():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.updatepost(i_data['string'], i_data)
    else:
        return main_error

@app.route('/updateuser', methods=['POST'])
def update_user():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.updateuser(i_data['string'], i_data)
    else:
        return main_error

@app.route('/updatecat', methods=['POST'])
def update_cat():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.updatecat(i_data['string'], i_data)
    else:
        return main_error

@app.route('/updaterole', methods=['POST'])
def update_role():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.updaterole(i_data['string'], i_data)
    else:
        return main_error

@app.route('/deletepost', methods=['POST'])
def delete_post():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.deletepost(i_data['string'], i_data)

@app.route('/deleterole', methods=['POST'])
def delete_role():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.deleterole(i_data['string'], i_data)

@app.route('/deletecat', methods=['POST'])
def delete_cat():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.deletecat(i_data['string'], i_data)

@app.route('/deleteuser', methods=['POST'])
def delete_user():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.deleteuser(i_data['string'], i_data)

@app.route('/createuser', methods=['POST'])
def create_user():
    if request.data:
        i_data = json.loads(request.data)
        return BaseClass.createuser(i_data['string'], i_data)
    else:
        return main_error


if __name__ == '__main__':
   app.run()