##from flask_cors import CORS, cross_origin
from classes.sendservice import SendServices

class BaseClass:
    @staticmethod
    def homepage(data):
        ##return string
        return SendServices.select(data)
        ##alldb = mycursor.execute("SHOW DATABASES")
        ##remember to send the request data as thus 'the table', 'the column', and 'the data'
        ##mycursor.execute("SHOW DATABASES")
        ##alldata = mycursor.fetchall()
        ##for x in alldata:
            ##print(x)
        
        ##    print("".join(x))
        ##pass
    
    @staticmethod
    def workpage(data):
        return SendServices.select(data)
    
    @staticmethod
    def workpagebycat(data):
        return SendServices.select(data)
    
    @staticmethod
    def usersbyrole(data):
        return SendServices.select(data)
        
    @staticmethod
    def getparentcat(data):
        return SendServices.select(data)
    
    @staticmethod
    def getcat(data):
        return SendServices.select(data)
    
    @staticmethod
    def getperms(data):
        return SendServices.select(data)

    @staticmethod
    def getroles(data):
        return SendServices.select(data)
    
    @staticmethod
    def getusers(data):
        return SendServices.select(data)
    
    @staticmethod
    def getsettings(data):
        return SendServices.select(data)
    
    @staticmethod
    def adminlogin(data):
        return SendServices.select(data)
    
    @staticmethod
    def mydashboard(data):
        return SendServices.select(data)
    
    @staticmethod
    def createcat(string, data):
        return SendServices.insert(string, data)
    
    @staticmethod
    def createpost(string, data):
        return SendServices.insert(string, data)
    
    @staticmethod
    def createuser(string, data):
        return SendServices.insert(string, data)
        
    @staticmethod
    def savesettings(string, data):
        return SendServices.update(string, data)
    
    @staticmethod
    def updatepost(string, data):
        return SendServices.update(string, data)
    
    @staticmethod
    def updateuser(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def updatecat(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def updaterole(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def deletepost(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def deleterole(string, data):
        return SendServices.update(string, data)
        
    @staticmethod
    def deletecat(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def deleteuser(string, data):
        return SendServices.update(string, data)
##p1 = Person("John", 36)