import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Auth from "./services/middleware/auth.js";

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: "/",
			name: "home",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: Home
		},
		{
			path: "/about",
			name: "about",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/About.vue")
		},
		{
			path: "/contact",
			name: "contact",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/Contact.vue")
		},
		{
			path: "/ourwork",
			name: "ourwork",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/OurWork.vue")
		},
		{
			path: "/myadmin",
			name: "myadmin",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/MyAdmin.vue")
		},
		{
			path: "/mydashboard",
			name: "mydashboard",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/MyDashboard.vue")
		},
		{
			path: "/createcategory",
			name: "createcategory",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/CreateCategory.vue")
		},
		{
			path: "/createuser",
			name: "createuser",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/CreateUser.vue")
		},
		
		{
			path: "/viewusers",
			name: "viewusers",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/ViewUsers.vue")
		},
		{
			path: "/settings",
			name: "settings",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/Settings.vue")
		},
		{
			path: "/categories",
			name: "categories",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/Categories.vue")
		},
		{
			path: "/permissions",
			name: "permissions",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/Permissions.vue")
		},
		{
			path: "/newpost",
			name: "newpost",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/NewPost.vue")
		},
		{
			path: "/edituser",
			name: "edituser",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/EditUser.vue")
		},
		{
			path: "/editcategory",
			name: "editcategory",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/EditCategory.vue")
		},
		{
			path: "/roles",
			name: "roles",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/Roles.vue")
		},
		{
			path: "/editrole",
			name: "editrole",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/EditRole.vue")
		},
		{
			path: "/editpost",
			name: "editpost",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/EditPost.vue")
		},
		{
			path: "/allposts",
			name: "allposts",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Admin(options);
			},
			meta:{
				middleware: true
			},
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/AllPosts.vue")
		}

	]
});
