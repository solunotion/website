import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: "/",
			name: "home",
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/Home.vue")
		},
		{
			path: "/about",
			name: "about",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import("./views/About.vue")
		},
		{
			path: "/contact",
			name: "contact",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/Contact.vue")
		},
		{
			path: "/ourwork",
			name: "ourwork",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			import("./views/OurWork.vue")
		},
		{
			path: "/login",
			name: "login",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/Login.vue")
		},
		{
			path: "/createcategory",
			name: "createcategory",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/CreateCategory.vue")
		},
		{
			path: "/categories",
			name: "categories",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/Categories.vue")
		},
		{
			path: "/searchbyservices",
			name: "searchbyservices",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/SearchByServices.vue")
		},
		{
			path: "/searchbycompany",
			name: "searchbycompany",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/SearchByCompany.vue")
		},
		,
		{
			path: "/companywork",
			name: "companywork",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/CompanyWork.vue")
		},
		{
			path: "/services",
			name: "services",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/Services.vue")
		},
		{
			path: "/companies",
			name: "companies",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/Companies.vue")
		},
		{
			path: "/dashboard",
			name: "dashboard",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/Dashboard.vue")
		},
		{
			path: "/allposts",
			name: "allposts",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/AllPosts.vue")
		},
		{
			path: "/createpost",
			name: "createpost",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
			  import(/* webpackChunkName: "about" */ "./views/CreatePost.vue")
		}
	]
});
