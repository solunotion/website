import axios from 'axios';

export default{
    name: "ApiRequest",
    //defaultURL: 'https://solunotion.com/',
    defaultURL: 'http://127.0.0.1:5000/',
    w_platform: "web",

    getAboutPage(){
        //return console.log(axios.post());
    },

    post(data){
        data['platform'] = this.w_platform;
        return axios({
            method: 'post',
            url:data.string,
            baseURL: this.defaultURL,
            data: data
        })
        .then(response=>{
            return response.data;
        });
    },

    get(i_data){
        i_data['platform'] = this.w_platform;
        return axios({
            method: 'post',
            url:i_data.string,
            baseURL: this.defaultURL,
            data: i_data
        })
        .then(response=>{
            return response.data;
        });
    }
};