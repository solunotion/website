import successJson from "@/json/localJson/successJson.json";
import errorJson from "@/json/localJson/errorJson.json";
import dbJson from "@/json/localJson/dbJson.json";

export default{
    name: "LocalApi",
    successJson, errorJson, dbJson,

    post(string, data){
        if(string == "front/signup.php"){
            let response = Promise.resolve(this.successJson.guest.signup);
            //console.log("yes we are ready to signup", response);
            return response;
        }

        if(string == "front/login.php"){
            let response = Promise.resolve(this.successJson.guest.login);
            return response;
        }

        if(string == "user/checkbook.php"){
            let response = Promise.resolve(this.successJson.reader.checkbook);
            return response;
        }

        if(string == "user/createbook.php"){
            let response = Promise.resolve(this.successJson.reader.createbook);
            return response;
        }
        if(string == "user/publish.php"){
            let response = Promise.resolve(this.successJson.reader.publish);
            return response;
        }
        if(string == "user/withdrawearnings.php"){
            let response = Promise.resolve(this.successJson.publisher.withdraw);
            return response;
        }

        if(string == "user/myreads.php"){
            let response = Promise.resolve(this.successJson.reader.myreads);
            return response;
        }

        if(string == "user/mybooks.php"){
            let response = Promise.resolve(this.successJson.publisher.mybooks);
            return response;
        }

        if(string == "front/store.php"){
            let response = Promise.resolve(this.successJson.guest.storebooks);
            return response;
        }

        if(string == "user/profile.php"){
            let response = Promise.resolve(this.successJson.reader.profile);
            return response;
        }

        if(string == "user/getbanks.php"){
            let response = Promise.resolve(this.successJson.reader.banks);
            return response;
        }

        if(string == "front/popular.php"){
            let response = Promise.resolve(this.successJson.guest.popularbooks);
            return response;
        }

        if(string == "front/catlang.php"){
            let response = Promise.resolve(this.successJson.guest.catlang);
            return response;
        }
    },

    postimg(string, data){
        if(string == "user/createbook.php"){
            let response = Promise.resolve(this.successJson.reader.createbook);
            return response;
        }
    },
    get(data){
        if(data.string == "/"){
            //console.log("this is our data", data);
            return Promise.resolve(this.successJson.guest.recentworks);
            //console.log("Yes we are here");
        }

        if(data.string == "ourwork"){
            return Promise.resolve(this.successJson.guest.ourworks.onsuccess);
        }

        if(data.string == "ourworkbycat"){
            if(data.cat_id == 0){
                console.log("our category is all and we will return this", this.successJson.guest.ourworks.onsuccess)
                return Promise.resolve(this.successJson.guest.ourworks.onsuccess);
            }

            if(data.cat_id > 0){
                var response = this.successJson.guest.ourworks.onsuccess;
                var works = [];
                
                var our_object = response.data.works;
                var our_response = {
                    status: "success",
                    code: 200,
                    message: "Post gotten successfully",
                    data:{
                        base_img_url: response.data.base_img_url,
                        count: 1,
                        last_post: 1,
                        works: []
                    }
                }
                for (const key in our_object) {
                    if (our_object.hasOwnProperty(key)) {
                        if(Number(our_object[key].post_category_id) == data.cat_id){
                            works.push(our_object[key]);
                        }
                    }
                }
                if(works.length > 0){
                    our_response.data.works = works;
                    return Promise.resolve(our_response);
                }else{
                    //console.log("this is our works", works);
                    return Promise.resolve(this.successJson.guest.ourworks.onerror);
                }
            }
        }
        if(data.string = "getparentcat"){
            return Promise.resolve(this.successJson.guest.workcategories);
        }
    }
}