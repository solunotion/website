//import successJson from "@/json/data.json";
import SendService from "@/services/controller/sendService.js";

//import axios from 'axios';
//import axios from '@/plugins/axios';

export default {
    
    name: "AuthApi",
    SendService,
    //defaultURL: process.env.BASE_URL+'server/api/controller/',
    //successJson,
    
    userType() {        
        if (localStorage.getItem("usertype") === null) {
            return "guest";
        }
        if (localStorage.getItem("usertype") === "guest") {
            return "guest";
        }
        if (localStorage.getItem("usertype") === "admin") {
            return "admin";
        }
    },
    guestUser(){
        if(this.userType() == "guest"){
            return true;
        }else{
            return false;
        }
    },

    adminLoggedIn(){
        if(this.userType() == "admin"){
            return true;
        }else{
            return false;
        }
    },

    userLoggedIn(){
        if(this.userType() == "admin"){
            return true;
        }else{
            return false;
        }
    },

    alert(type, message){
        if(type == "success"){
            swal({
                title: "Yea, Success",
                text: message,
                icon: "success"
            });
        }

        if(type == "error"){
            swal({
                title: "Nah...",
                text: message,
                icon: "error"
            });
        }
        
        if(type == "info"){
            swal({
                title: "Hello",
                text: message,
                icon: "info"
            });
        }

        if(type == "warning"){
            swal({
                title: "Ahhh",
                text: message,
                icon: "warning"
            });
        }
    },

    getCategories(){
        let data={
            string: "getcat",
        };
        return this.SendService.get(data);
    },

    createCategory(data){
        data['string'] = 'createcat';
        return this.SendService.post(data);
    },
    createPost(data){
        data['string'] = 'createpost';
        return this.SendService.post(data);
    },
    saveSettings(data){
        data['string'] = 'savesettings';
        return this.SendService.post(data);
    },
    getSettings(){
        let data={
            string: "getsettings"
        };
        return this.SendService.get(data);
    },
    getParentCategories(){
        let data = {
            string: "getparentcat"
        }
        return this.SendService.get(data);
    },
    adminUser(){
        if(this.userType() == "admin"){
            return true;
        }else{
            return false;
        }
    },

    clearSession(){
        //alert('I am about to clear session');
        localStorage.clear();
    },

    anotherGuy(){
        alert("hello we are in Auth");
    },
    guestLogin(uname, pword) {
        console.log("this is your uname", uname);
        
        /*axios({
            method: 'post',
            url:'',
            baseURL: this.defaultURL,
            data: {
                post_title: 'guestlogin',
                phone: uname,
                pwd: pword
            }
        })
        .then(response=>{
            return response.data;
        });*/
    },

    logoutUser(){
        //alert("i am about to clear session");
        //localStorage.setItem("usertype", "guest");
        this.clearSession();
        return true;
        //localStorage.clear();
        //localStorage.setItem("usertype", "guest");
        //window.location.href = "";
        //this.push('login');
        //resolve();
    },

    closeModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.close();
    },

    openModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.open();
    },

    myConsole(string, data){
        console.log(string, data);
    },
    closeModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.close();
    },

    openModal(data){
        let elem = document.getElementById(data);
        let instance = M.Modal.getInstance(elem);
        instance.open();
    },

    nonDismissibleModal(){
        let elem= document.querySelectorAll('.modal');
        let instance = M.Modal.init(elem, {dismissible: false});
    }
};