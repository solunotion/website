import SendService from "@/services/controller/sendService.js";

export default {
    
    name: "GuestBase",
    SendService,

    getAboutPg(){
        //return this.SendService.get("about");
    },

    getHomePg(){
        let data = {
            string:"/",
            g_from: "0"
        };
        return this.SendService.get(data);
    },
    
    getParentCategories(){
        let data={
            string: "getparentcat",
        };
        return this.SendService.get(data);
    },
    getWorkPg(f_data){
        /*let data = {
            string: "ourwork",
            g_from: f_data
        };
        */
       f_data['string'] = 'ourwork';
        return this.SendService.get(f_data);
    },
    myConsole(string, data){
        console.log(string, data);
    },

    getWorkByCat(m_data){
        /*let data = {
            string: "ourworkbycat",
            g_from: m_data['g_from'],
            cat_id: m_data['cat_id']
        };*/
        m_data['string'] = 'ourworkbycat';
        //console.log('this is the data i want to pass, the string, g_from and cat_id', data);
        return this.SendService.get(m_data);
    },

    anotherGuy(){
        //alert("hello");
    }
};