//import successJson from "@/json/data.json";
import SendService from "@/services/controller/sendService.js";

//import axios from 'axios';
//import axios from '@/plugins/axios';

export default {
    
    name: "MainBase",
    SendService,
    //defaultURL: process.env.BASE_URL+'server/api/controller/',
    //successJson,
    
    userType() {
        let u_t = this.getSessionItem('process_data');
        if (u_t === null || u_t === undefined) {
            return "Guest";
        }
        if (u_t && u_t.data.user.role == 'Guest') {
            return "Guest";
        }
        if (u_t && u_t.data.user.role == 'Admin') {
            return "Admin";
        }
    },
    guestUser(){
        if(this.userType() == "Guest"){
            return true;
        }else{
            return false;
        }
    },

    adminLoggedIn(){
        if(this.userType() == 'Admin'){
            return true;
        }else{
            return false;
        }
    },

    userLoggedIn(){
        if(this.userType() == 'User'){
            return true;
        }else{
            return false;
        }
    },
    scrollTo(data){
        if(data == "top"){
            setTimeout(function(){
                window.scroll({
                    top: 0,
                    left: 0,
                    behavior: 'smooth'
                });
            }, 500);
        }
        if(data == "bottom"){
            window.scroll({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    },

    alert(type, message){
        if(type == "success"){
            swal({
                title: message,
                icon: "success"
            });
        }

        if(type == "error"){
            swal({
                title: message,
                icon: "error"
            });
        }
        
        if(type == "info"){
            swal({
                title: message,
                icon: "info"
            });
        }

        if(type == "warning"){
            swal({
                title: message,
                icon: "warning"
            });
        }
    },
    
    setSessionItem(key, value){
        localStorage.setItem(key, value);
    },

    getSessionItem(key){
        return JSON.parse(localStorage.getItem(key));
    },
    getPermissions(){
        let data={
            string: "getperms",
        };
        return this.SendService.get(data);
    },
    getRoles(){
        let data={
            string: "getroles",
        };
        return this.SendService.get(data);
    },
    getCategories(){
        let data={
            string: "getcat",
        };
        return this.SendService.get(data);
    },

    createCategory(data){
        data['string'] = 'createcat';
        return this.SendService.post(data);
    },
    createPost(data){
        data['string'] = 'createpost';
        return this.SendService.post(data);
    },
    updatePost(data){
        data['string'] = 'updatepost';
        return this.SendService.post(data);
    },
    updateCategory(data){
        data['string'] = 'updatecat';
        return this.SendService.post(data);
    },
    updateRole(data){
        data['string'] = 'updaterole';
        return this.SendService.post(data);
    },
    deletePost(data){
        data['string'] = 'deletepost';
        return this.SendService.post(data);
    },
    deleteRole(data){
        data['string'] = 'deleterole';
        return this.SendService.post(data);
    },
    deleteCat(data){
        data['string'] = 'deletecat';
        return this.SendService.post(data);
    },
    deleteUser(data){
        data['string'] = 'deleteuser';
        return this.SendService.post(data);
    },
    createUser(data){
        data['string'] = 'createuser';
        return this.SendService.post(data);
    },
    adminStat(data){
        data['string'] = 'adminstat';
        return this.SendService.post(data);
    },
    saveSettings(data){
        data['string'] = 'savesettings';
        return this.SendService.post(data);
    },
    dashboardStat(data){
        data['string'] = 'mydashboard'
        return this.SendService.post(data);
    },
    getSettings(){
        let data={
            string: "getsettings"
        };
        return this.SendService.get(data);
    },
    getParentCategories(){
        let data = {
            string: "getparentcat"
        }
        return this.SendService.get(data);
    },
    getRoles(){
        let data = {
            string: "getroles"
        }
        return this.SendService.get(data);
    },
    updateUser(data){
        data['string'] = 'updateuser';
        return this.SendService.post(data);
    },
    getUsers(data){
        data['string'] = "getusers";
        return this.SendService.get(data);
    },
    getUsersByRole(data){
        data['string'] = 'usersbyrole';
        return this.SendService.get(data);
    },
    adminUser(){
        if(this.userType() == "admin"){
            return true;
        }else{
            return false;
        }
    },

    clearSession(){
        localStorage.clear();
    },

    anotherGuy(){
        alert("hello we are in Auth");
    },
    guestLogin(d_data) {
        f_data['string'] = 'guestlogin';
        return this.SendService.get(f_data);
        console.log("this is your uname", uname);
        
        /*axios({
            method: 'post',
            url:'',
            baseURL: this.defaultURL,
            data: {
                post_title: 'guestlogin',
                phone: uname,
                pwd: pword
            }
        })
        .then(response=>{
            return response.data;
        });*/
    },

    adminLogin(d_data){
        d_data['string'] = 'adminlogin';
        return this.SendService.post(d_data);
    },

    logoutUser(){
        //alert("i am about to clear session");
        //localStorage.setItem("usertype", "guest");
        this.clearSession();
        return true;
        //localStorage.clear();
        //localStorage.setItem("usertype", "guest");
        //window.location.href = "";
        //this.push('login');
        //resolve();
    },

    closeModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.close();
    },

    openModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.open();
    },

    myConsole(string, data){
        console.log(string, data);
    },
    closeModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.close();
    },

    openModal(data){
        let elem = document.getElementById(data);
        let instance = M.Modal.getInstance(elem);
        instance.open();
    },

    nonDismissibleModal(){
        let elem= document.querySelectorAll('.modal');
        let instance = M.Modal.init(elem, {dismissible: false});
    },
    getAboutPg(){
        //return this.SendService.get("about");
    },

    getHomePg(){
        let data = {
            string:"/",
            g_from: "0"
        };
        return this.SendService.get(data);
    },
    
    getParentCategories(){
        let data={
            string: "getparentcat",
        };
        return this.SendService.get(data);
    },
    getWorkPg(f_data){
        /*let data = {
            string: "ourwork",
            g_from: f_data
        };
        */
       f_data['string'] = 'ourwork';
       return this.SendService.get(f_data);
    },
    myConsole(string, data){
        console.log(string, data);
    },

    getWorkByCat(m_data){
        /*let data = {
            string: "ourworkbycat",
            g_from: m_data['g_from'],
            cat_id: m_data['cat_id']
        };*/
        m_data['string'] = 'ourworkbycat';
        //console.log('this is the data i want to pass, the string, g_from and cat_id', data);
        return this.SendService.get(m_data);
    },

    anotherGuy(){
        //alert("hello");
    }
};