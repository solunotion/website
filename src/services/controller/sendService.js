//import ApiRequest from "@/services/api/localApi.js";
import ApiRequest from "@/services/api/api.js";

export default {
    
    name: "SendService",
    ApiRequest,

    post(data){
        //console.log("i am in post of sendservice");
        return ApiRequest.post(data);
        //this.ApiRequest.post(string, data);
    },

    get(data){
        return ApiRequest.get(data);
    }
};