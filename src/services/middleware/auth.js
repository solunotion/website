import MainBase from "@/services/controller/MainBase.js";

export default  {

    name: "Auth",
    Admin(options){
        if(MainBase.guestUser()){
            return options.next(options.from.fullPath);
        }

        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                if(MainBase.adminLoggedIn()){
                    return options.next();    
                }else{
                    return options.next(options.from.fullPath);
                }
            }else{
                return options.next(options.from.fullPath);
            }
        }
    },

    Guest(options){
        if(MainBase.merchantLoggedIn()){
            return options.next(options.from.fullPath);
        }
        if(MainBase.h_merchantLoggedIn()){
            return options.next(options.from.fullPath);
        }

        if(MainBase.staff_merchantLoggedIn()){
            return options.next(options.from.fullPath);
        }

        if(MainBase.guestUser()){
            return options.next();
        }else{

        }
    },

    Everyone(options){
        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                return options.next();
            }else{
                return options.next(options.from.fullPath);
            }
        }
    },

    User(options){
        if(MainBase.guestUser()){
            return options.next(options.from.fullPath);
        }

        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                if(MainBase.userLoggedIn()){
                    return options.next();    
                }else{
                    return options.next(options.from.fullPath);
                }
            }else{
                return options.next(options.from.fullPath);
            }
        }
    }
}