import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		process_data: {},
		edit_data: {},
		app_name: "SoluNotion",
		online_stat: null,
		active_stat: null
	},

	getters:{
		PROCESS_DATA_G: function(state){
			return state.process_data;
		},
		ONLINE_STAT_G: function(state){
			return state.online_stat;
		},
		APP_NAME: function(state){
			return state.app_name;
		},
		EDIT_DATA_G: function(state){
			return state.edit_data;
		}
	},
	
	mutations: {
		UPDATE_PROCESS_DATA_M: function(state, data){
			state.process_data = data;
			localStorage.setItem("process_data", JSON.stringify(data));
		},
		LOGOUT_USER_M: function(state){
			state.active_stat = false;
			state.process_data = {};
			localStorage.clear();
			//window.location.href = "";
		},
		UPDATE_EDIT_DATA_M: function(state, data){
			state.edit_data = data;
			localStorage.setItem("edit_data", JSON.stringify(data));
		}
	},
	
	actions: {
		logoutUser_A: function(context){
			context.commit("LOGOUT_USER_M");
		},
		updateProcessData_A: function(context, data){
			context.commit("UPDATE_PROCESS_DATA_M", data);
		},
		updateEditData_A: function(context, data){
			context.commit("UPDATE_EDIT_DATA_M", data);
		}
	}
});
